// Теоретичне питання
// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

// Завдання
// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.
// Необов'язкове завдання підвищеної складності
// Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.




const load = document.querySelector("#loadMovies");
load.addEventListener("click", reqMovies);
const URL = "https://ajax.test-danit.com/api/swapi/films"

async function reqMovies (){
    const response = await fetch(`${URL}`);
    const data = await response.json();
    const movies = document.querySelector('#movies');

   data
     .sort((a, b) => a.episodeId - b.episodeId)
     .map(movie => {

         // Movies
         const div = document.createElement("div");
         const ul = document.createElement("ul");
         const title = document.createElement("h2");
         const paragraph = document.createElement("p");
         const preload = document.createElement("div");
         preload.id = 'preloader';
         preload.classList.add('visibility')
         const {episodeId, name, openingCrawl} = movie;
         title.textContent = `Episode ${episodeId} ${name}`;
         paragraph.textContent = openingCrawl;
         div.append(title);
         div.append(preload);
         div.append(paragraph);
         div.append(ul);
         movies.append(div);

        // Characters
         movie.characters.forEach( async function (character){
             const li = document.createElement("li");
             const resolve = await fetch(character);
             const data = await resolve.json();
             li.textContent = data.name;
             ul.append(li);
         })
     })
     .join("")
};


//Preloaderg

function loadData() {
    return new Promise((resolve, reject) => {
        // ?? code ??
    })
  }
  
  loadData()
    .then(() => {
      let preloaderEl = document.getElementById('preloader');
      preloaderEl.classList.remove('visibility')
      preloaderEl.classList.add('hidden');
    });